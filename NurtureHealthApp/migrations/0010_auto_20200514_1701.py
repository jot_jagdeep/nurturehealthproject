# Generated by Django 3.0.4 on 2020-05-14 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NurtureHealthApp', '0009_auto_20200514_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='status',
            field=models.BooleanField(default=True),
        ),
    ]
